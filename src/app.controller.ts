import { Controller, Get, Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { Message } from './message.event';

@Controller()
export class AppController {
  constructor(@Inject('USERS_SERVICE') private readonly client: ClientProxy) { }

  async onApplicationBootstrap() {
    await this.client.connect();
  }

  @Get()
  getHello(): string {
    this.client.emit<any>('createuser', new Message('Event Payload'));
    return 'Event Emitted';
  }
}